import com.yagocarballo.database.MySQL_Handler;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Properties;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestConnection {
    private final Properties prop = new Properties();
    private MySQL_Handler db;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        System.out.println("-- Testing Settings Exist");
        prop.load(TestConnection.class.getResourceAsStream("/settings.properties"));
        assertTrue(true);

        System.out.println("-- Testing Break Switch");
        assertEquals("Trying to Break the Test: ", true, true);

    }

    @Test
    public void testConnection () throws Exception {
        System.out.println("-- Testing Database Connection");
        db = new MySQL_Handler(prop);
        assertEquals("Testing Connection to Database: ", true, db.connect());
    }
}
