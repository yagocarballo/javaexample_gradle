package com.yagocarballo.app;

import com.yagocarballo.database.MySQL_Handler;

import java.io.IOException;
import java.util.Properties;

public class Start {
    private final Properties prop = new Properties();
    private MySQL_Handler db;

    public static void main (String[] args) { new Start(); }

    public Start () {
        try {
            prop.load(Start.class.getResourceAsStream("/settings.properties"));
            db = new MySQL_Handler(prop);
            db.connect();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}