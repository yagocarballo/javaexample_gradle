package com.yagocarballo.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MySQL_Handler {
    private final Properties prop;
    private final String username;
    private final String password;
    private final String database;
    private final String host;
    private final String port;

    public MySQL_Handler (Properties prop) {
        this.prop       = prop;
        this.username   = this.prop.getProperty("username");
        this.password   = this.prop.getProperty("password");
        this.database   = this.prop.getProperty("database");
        this.host       = this.prop.getProperty("host");
        this.port       = this.prop.getProperty("port");

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean connect () {
        try {
            String url = String.format("jdbc:mysql://%s:%s/%s", host, port, database);
            System.out.println(url);
            Connection conn = DriverManager.getConnection(url, username, password);
            System.out.println(conn.toString());
            conn.close();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}