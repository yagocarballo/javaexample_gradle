## Test Java Project

This is a Sample Java Project for Gradle, using the default layout.

####Tasks:
1. run      -> Runs the Project
2. test     -> Runs the JUnit Test Cases
3. check    -> Checks the Integrity of the Gradle Script
4. distZip  -> Creates a Distribution Zip File
5. distTar  -> Creates a Distribution Tar File
6. assemble -> Builds the Project
7. idea     -> Generates an IntelliJ Project
8. eclipse  -> Generates an Eclipse Project



|                             Notes:                            |
| ------------------------------------------------------------- |
| The Default build Folder is called build and is in the root   |
| The builds are stored inside build/libs                       |
| The JUnit Reports are Stored inside build/reports             |
| The Distribution Files are Stored inside build/distribution   |
